import { GET_ITEMS, RATE_ITEM } from "./types";
import axios from "axios";

export const getItems = active => dispatch => {
  if (active === "movies") {
    axios.get("/api/movies").then(res => {
      dispatch({
        type: GET_ITEMS,
        payload: res.data
      });
    });
  } else {
    axios.get("/api/shows").then(res => {
      dispatch({
        type: GET_ITEMS,
        payload: res.data
      });
    });
  }
};

export const rateItem = (id, rating) => dispatch => {
  const payload = { id: id, rating: rating };
  dispatch({
    type: RATE_ITEM,
    payload: payload
  });
};
