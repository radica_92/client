import { combineReducers } from "redux";
import movieReducer from "./movieReducer";
import userReducer from "../reducers/userReducer";

export default combineReducers({
  movie: movieReducer,
  user: userReducer
});
