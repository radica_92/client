import { REGISTER_USER, LOGIN } from "../actions/types";
import isEmpty from "is-empty";

const initialState = {
  user: {},
  isAuthenticated: false
};

export default function(state = initialState, action) {
  console.log("payload", action.payload);

  switch (action.type) {
    case REGISTER_USER:
      return {
        ...state,
        user: action.payload
      };
    case LOGIN:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };
    default:
      return state;
  }
}
