import { GET_ITEMS, RATE_ITEM } from "../actions/types";

const initialState = {
  items: []
};

export default function(state = initialState, action) {
  console.log("payload111", action.payload);

  switch (action.type) {
    case GET_ITEMS:
      return {
        ...state,
        items: action.payload
      };
    case RATE_ITEM:
      console.log("payload", action.payload.id);
      return {
        ...state,
        items: state.items.map(item =>
          item._id === action.payload.id
            ? { ...item, rating: action.payload.rating }
            : item
        )
      };
    default:
      return state;
  }
}
