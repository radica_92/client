import React, { Component } from "react";
import Item from "./Item";
import { connect } from "react-redux";
import { getItems } from "../actions/movieActions";
import Header from "./Header";

class Home extends Component {
  state = {
    activeTab: "movies"
  };

  componentDidMount = async () => {
    await this.props.getItems(this.state.activeTab);
  };
  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.activeTab !== this.state.activeTab) {
      this.props.getItems(this.state.activeTab);
    }
  };

  toggleActive = () => {
    if (this.state.activeTab === "movies") {
      this.setState({ activeTab: "shows" });
    } else {
      this.setState({ activeTab: "movies" });
    }
  };

  render() {
    return (
      <div>
        <Header
          toggleActive={this.toggleActive}
          activeTab={this.state.activeTab}
        />
        <Item />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  movies: state.movie
});

export default connect(
  mapStateToProps,
  { getItems }
)(Home);
